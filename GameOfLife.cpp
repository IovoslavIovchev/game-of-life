#include "GameOfLife.h"

bool const GameOfLife::IsAlive(Grid const& grid, int r, int c) {
  if (r < 0 || r > grid.Rows || c < 0 || c > grid.Columns) {
    return false;
  }

  return grid.Buffer[r * grid.Columns + c];
}

int const GameOfLife::AliveNeightbours(Grid const& grid, int r, int c) {
  int alive = 0;
  for (int i = -1; i < 2; i++) {
    for (int j = -1; j < 2; j++) {
      if (IsAlive(grid, r + i, c + j)) {
        ++alive;
      }
    }
  }

  alive -= IsAlive(grid, r, c);

  return alive;
}

GameOfLife::Grid const GameOfLife::CalculateNextGeneration(Grid const& grid) {
  bool* temp = new bool[grid.Rows * grid.Columns]();

  for (int r = 0; r < grid.Rows; r++) {
    for (int c = 0; c < grid.Columns; c++) {
      int const neighbours = AliveNeightbours(grid, r, c);
      int const current_index = r * grid.Columns + c;

      if (neighbours < 2 || neighbours > 3) {
        temp[current_index] = false;
      } else if (neighbours == 3) {
        temp[current_index] = true;
      } else {
        temp[current_index] = grid.Buffer[current_index];
      }
    }
  }

  return Grid(grid.Rows, grid.Columns, temp);
}
