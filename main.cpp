#include <algorithm>
#include <functional>
#include <iostream>

#include "GameOfLife.h"

void PlayGame(GameOfLife::Grid const& grid) {
  std::function<void(GameOfLife::Grid const&, int const)> f =
      [&](GameOfLife::Grid const& grid, int const generation) {
        std::cout << "Generation " << generation << ':' << std::endl;

        std::cout << grid << std::endl;

        std::cout << "Press any key to go to next generation." << std::endl
                  << std::endl;

        std::cin.get();

        f(GameOfLife::CalculateNextGeneration(grid), generation + 1);
      };

  f(grid, 1);
}

int main() {
  bool* buf = new bool[100]();
  buf[0] = true;
  buf[21] = true;
  buf[22] = true;
  buf[34] = true;

  auto grid = GameOfLife::Grid(10, 10, buf);

  PlayGame(grid);
}
