#pragma once

#include <iostream>
#include <memory>

class GameOfLife final {
 public:
  struct Grid {
    int const Rows;
    int const Columns;
    bool const* const Buffer;

    Grid(int const& rows, int const& cols)
        : Grid(rows, cols, new bool[rows * cols]) {}

    Grid(int const& rows, int const& cols, bool const* const buf)
        : Rows(rows), Columns(cols), Buffer(buf) {}

    friend std::ostream& operator<<(std::ostream& stream, Grid const& grid) {
      for (int r = 0; r < grid.Rows; r++) {
        for (int c = 0; c < grid.Columns; c++) {
          stream << (grid.Buffer[r * grid.Columns + c] ? "o" : ".") << ' ';
        }

        stream << std::endl;
      }

      return stream;
    }
  };

  static Grid const CalculateNextGeneration(Grid const&);

 private:
  static bool const IsAlive(Grid const&, int, int);
  static int const AliveNeightbours(Grid const&, int, int);
};
